# 360Project
Assembly Language emulator in Java 

The file SimX86.java is entry point of the program.  When run the program will ask the user for the path to a .asm file to run.  The directory 'files' contains several working programs that can be used for testing.  An example would be files/AddSquare.asm.  Once the asm file is loaded into the program the user will be prompted for a command.  If an empty string is supplied a menu containing all the command options will be displayed.